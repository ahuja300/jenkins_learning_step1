*** Settings ***
Documentation    This is just for testing continue
Test Setup      TestLevel_Open
Test Teardown       Test_level_close testcase


*** Test Cases ***
# if in a tc we have defined setup and Teardown then it will overrode the suite level setup and tear down
TC01
    [Setup]  Indi_setup
    [Teardown]     Indi_teardown
    log to console  inside testcase1

TC02
    log to console  inside testcase2



*** Keywords ***
TestLevel_Open
    log to console  Test level Set up
Test_level_close testcase
    log to console  test level teardown
Indi_setup
    log to console  Individual test set up
Indi_teardown
    log to console  Individual tear down

*** Variables ***
