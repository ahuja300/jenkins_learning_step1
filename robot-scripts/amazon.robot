*** Settings ***
Documentation       this is a test

Library  SeleniumLibrary
Library  XML

*** Test Cases ***
TC01
    [Documentation]  testing
    [Tags]  smoke
    #open browser    https://google.com      chrome
    ${root}=    parse xml    C:/development/robot-scripts/first-script/data.xml

#    :FOR     ${i}    IN RANGE  1     5
#    \    ${food}=    get element  ${root}    food[${i}]/description
#    \   log to console  ${food.text}
#    ${food}=    get element  ${root}    food/description
#    log to console  ${food}
#    log to console  ${count}
#   ${element}=   get element  ${root}    food[1]
  # element attribute should be      ${root}     food[1]/price       $5.95
   @{price}=    get elements texts   ${root}   food/price
   log to console  @{price}[3]