
*** Settings ***
Documentation       practice
Library  SeleniumLibrary
Library  DateTime
Library  String


# a new line added when we are creating a new branch in git local so editing a file just to check 


*** Test Cases ***
TimeSetting
    ${time}=     get time   timestamp   UTC
    ${time}=    replace string  ${time}    :   _
    ${time}=    replace string  ${time}    ${SPACE}   _
    log to console  ${time}

Verifying Title
    OPEN BROWSER   about:blank   ${browser}

    go to  https://rahulshettyacademy.com/AutomationPractice/

    maximize browser window
#    title should be     Rahul Shetty Academy
    element text should be   //h1[text()='Practice Page']       Practice Page
    close browser

Dropdown
    OPEN BROWSER   about:blank   ${browser}
    go to    https://rahulshettyacademy.com/angularpractice/

    select from list by index       //*[@id='exampleFormControlSelect1']     1
    select from list by index     //*[@id='exampleFormControlSelect1']     0
    close browser

Cheking text or substring from a UI Text
    OPEN BROWSER   about:blank   ${browser}
    go to    https://rahulshettyacademy.com/angularpractice/
    ${data}     get text    //*[contains(text(),'This is a dem')]
    should contain  ${data}     demo eCommerce web appplication
    log to console  ${data}
#    element text should be      //*[contains(text(),'This is a dem')]       This is a demo eCommerce web appplication developed using Angular 5 to help QAClick Academy students learn Protractor framework for testing Angular applications.
    close browser
Getting web elements from the UI and selecting one with the condiiton
    OPEN BROWSER   about:blank   ${browser}
    go to    https://rahulshettyacademy.com/AutomationPractice/
#    @{data}=    o  //*[@type='checkbox']
    @{data}=    Get WebElements    //*[@type='checkbox']
    : FOR  ${i}  IN  @{data}
    \    ${value}=   get element attribute   ${i}   value
    \    log to console  ${value}
    \    run keyword if    '${value}'=='option3'   click element  ${i}
    \    ...  ELSE   continue for loop
    \    checkbox should be selected   ${i}
    \    pass execution     checkbox value is seleted

Alert example
    OPEN BROWSER   about:blank   ${browser}
    go to    https://rahulshettyacademy.com/AutomationPractice/
    click element   //*[@id='alertbtn']
    ${mes}      handle alert        action=DISMISS
    log to console      ${mes}


multiple window handles
     OPEN BROWSER   about:blank   ${browser}
     go to    https://the-internet.herokuapp.com/redirector
     click link  Elemental Selenium
     click link  Elemental Selenium
     @{child}   get window handles
     : FOR  ${i}  IN    @{child}
     \      log to console     ${i}
     \      select window  ${i}
     \      ${url}=     get location
     \      log to console  ${url}
     \      select window   MAIN
     \      ${url}=     get location
     \      log to console  ${url}
     \      log to console  just chekcing



*** Variables ***

${browser}      Chrome
${value}